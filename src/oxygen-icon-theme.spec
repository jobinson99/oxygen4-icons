#
# spec file for package oxygen-icon-theme
#
# Copyright (c) 2015 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           oxygen4-icon-theme
Version:        15.04.0
Release:        4.1
Summary:        Oxygen4 Icon Theme
License:        LGPL-2.1+
Group:          System/GUI/KDE
Url:            http://www.kde.org
Source0:        oxygen-icons-%{version}.tar.xz
Source1:        22x22-package-manager-icon.png
Source2:        32x32-package-manager-icon.png
Source3:        48x48-package-manager-icon.png
Source4:        22x22_folder-html.png
Source5:        32x32_folder-html.png
Source6:        48x48_folder-html.png
Source7:        64x64_folder-html.png
Source8:        128x128_folder-html.png
Source9:        256x256_folder-html.png
Source10:       16x16_folder-html.png
BuildRequires:  cmake
BuildRequires:  fdupes
BuildRequires:  kde4-filesystem
BuildRequires:  libqt4-devel
BuildRequires:  xz
Requires:       hicolor-icon-theme
Recommends:     oxygen-icon-theme-large
Provides:       kdelibs4-icons = 3.92.0
Obsoletes:      kdelibs4-icons < 3.92.0
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildArch:      noarch

%description
This package contains the non-scalable icons of the Oxygen icon theme.

%prep
%setup -q -n oxygen-icons-%{version}

%build
  %cmake_kde4 -d build
  %make_jobs

%install
  cd build
  make DESTDIR=%{buildroot} install
  install -D -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/icons/oxygen/22x22/apps/package-manager-icon.png
  install -D -m 0644 %{SOURCE2} %{buildroot}%{_datadir}/icons/oxygen/32x32/apps/package-manager-icon.png
  install -D -m 0644 %{SOURCE3} %{buildroot}%{_datadir}/icons/oxygen/48x48/apps/package-manager-icon.png
  install -D -m 0644 %{SOURCE4} %{buildroot}%{_datadir}/icons/oxygen/22x22/places/folder-html.png
  install -D -m 0644 %{SOURCE5} %{buildroot}%{_datadir}/icons/oxygen/32x32/places/folder-html.png
  install -D -m 0644 %{SOURCE6} %{buildroot}%{_datadir}/icons/oxygen/48x48/places/folder-html.png
  install -D -m 0644 %{SOURCE7} %{buildroot}%{_datadir}/icons/oxygen/64x64/places/folder-html.png
  install -D -m 0644 %{SOURCE8} %{buildroot}%{_datadir}/icons/oxygen/128x128/places/folder-html.png
  install -D -m 0644 %{SOURCE9} %{buildroot}%{_datadir}/icons/oxygen/256x256/places/folder-html.png
  install -D -m 0644 %{SOURCE10} %{buildroot}%{_datadir}/icons/oxygen/16x16/places/folder-html.png

for i in 16x16 22x22 32x32 48x48 64x64 128x128 256x256;
do
install -D -m 0644 %{buildroot}%{_datadir}/icons/oxygen/${i}/places/folder-html.png %{buildroot}%{_datadir}/icons/oxygen/${i}/places/folder_html.png
install -D -m 0644 %{buildroot}%{_datadir}/icons/oxygen/${i}/places/folder-documents.png %{buildroot}%{_datadir}/icons/oxygen/${i}/apps/document.png;
done

cp -r ../scalable %{buildroot}%{_datadir}/icons/oxygen

  %fdupes %{buildroot}

%package scalable
Summary:        Oxygen Icon Theme
Group:          System/GUI/KDE
Requires:       oxygen-icon-theme = %{version}
Provides:       kdelibs4-icons-scalable = 3.92.0
Obsoletes:      kdelibs4-icons-scalable < 3.92.0

%description scalable
This package contains the scalable icons of the Oxygen icon theme.

%files scalable
%defattr(-,root,root)
%_kde4_iconsdir/oxygen/scalable

%package large
Summary:        Oxygen4 Icon Theme
Group:          System/GUI/KDE
Requires:       oxygen-icon-theme = %{version}

%description large
This package contains the large (128x128 and larger) non-scalable icons of the Oxygen icon theme.

%files large
%defattr(-,root,root)
%_kde4_iconsdir/oxygen/128x128
%_kde4_iconsdir/oxygen/256x256

%files
%defattr(-,root,root)
%exclude %_kde4_iconsdir/oxygen/scalable
%exclude %_kde4_iconsdir/oxygen/128x128
%exclude %_kde4_iconsdir/oxygen/256x256
%_kde4_iconsdir/oxygen

%changelog
* Sat Apr 11 2015 tittiatcoke@gmail.com
- Update to KDE Applications 15.04.0
  * KDE Applications 15.04.0
  * https://www.kde.org/announcements/announce-applications-15.04.0.php
* Sat Mar 21 2015 tittiatcoke@gmail.com
- Update to KDE Applications 15.03.95
  * KDE Applications 15.04 RC
* Sun Mar  8 2015 tittiatcoke@gmail.com
- Update to KDE Applications 15.03.80
  * KDE Applications 15.04 Beta
* Tue Mar  3 2015 tittiatcoke@gmail.com
- Update to KDE Applications 14.12.3
  * KDE Applications 14.12.3
  * See https://www.kde.org/announcements/announce-applications-14.12.3.php
* Sun Feb  1 2015 tittiatcoke@gmail.com
- Update to KDE Applications 14.12.2
  * KDE Applications 14.12.2
  * See https://www.kde.org/announcements/announce-applications-14.12.2.php
* Mon Jan 12 2015 tittiatcoke@gmail.com
- Update to KDE Applications 14.12.1
  * KDE Applications 14.12.1
  * See https://www.kde.org/announcements/announce-applications-14.12.1.php
* Wed Dec 17 2014 tittiatcoke@gmail.com
- Update to KDE Applications 14.12.0
  * KDE Applications 14.12.0
  * See https://www.kde.org/announcements/announce-applications-14.12.0.php
* Sun Nov  9 2014 tittiatcoke@gmail.com
- Update to 4.14.3
  * KDE 4.14.3 SC Bugfix Release
  * See http://www.kde.org/announcements/announce-4.14.3.php
* Sun Oct 12 2014 tittiatcoke@gmail.com
- Update to 4.14.2
  * KDE 4.14.2 SC Bugfix Release
  * See http://www.kde.org/announcements/announce-4.14.2.php
* Sat Sep 13 2014 tittiatcoke@gmail.com
- Update to 4.14.1
  * KDE 4.14.1 SC Bugfix Release
  * See http://www.kde.org/announcements/announce-4.14.1.php
* Fri Aug 15 2014 tittiatcoke@gmail.com
- Update to 4.14.0
  * KDE 4.14.0 SC Final Release
  * See http://www.kde.org/announcements/4.14/
* Thu Jul 17 2014 cgiboudeaux@gmx.com
- Update to 4.13.90
  * KDE 4.14 Beta 2 release
  * See http://www.kde.org/announcements/announce-4.14-beta2.php
* Thu Jul 10 2014 cgiboudeaux@gmx.com
- Update to 4.13.80
  * KDE 4.14 Beta 1 release
  * See http://www.kde.org/announcements/announce-4.14-beta1.php
* Sun Jun  8 2014 tittiatcoke@gmail.com
- Update to 4.13.2
  * KDE 4.13  release
  * See http://www.kde.org/announcements/announce-4.13.2.php
* Fri May  9 2014 tittiatcoke@gmail.com
- Update to 4.13.1
  * KDE 4.13.1  bug fix release
  * See http://www.kde.org/announcements/announce-4.13.1.php
* Fri Apr 11 2014 tittiatcoke@gmail.com
- Update to 4.13.0
  * KDE 4.13  release
  * See http://www.kde.org/announcements/4.13/
* Thu Mar 27 2014 tittiatcoke@gmail.com
- Update to 4.12.97
  * KDE 4.13 RC  release
  * See http://www.kde.org/announcements/announce-4.13-rc.php
* Thu Mar 20 2014 tittiatcoke@gmail.com
- Update to 4.12.95
  * KDE 4.13 Beta 3  release
  * See http://www.kde.org/announcements/announce-4.13-beta3.php
* Thu Mar 13 2014 tittiatcoke@gmail.com
- Update to 4.12.90
  * KDE 4.13 Beta 2  release
  * See http://www.kde.org/announcements/announce-4.13-beta2.php
* Fri Mar  7 2014 tittiatcoke@gmail.com
- Update to 4.12.80
  * KDE 4.13 Beta 1  release
  * See http://www.kde.org/announcements/announce-4.13-beta1.php
* Sat Feb  1 2014 tittiatcoke@gmail.com
- Update to 4.12.2
  * KDE 4.12.2  release
  * See http://www.kde.org/announcements/announce-4.12.2.php
* Sat Jan 11 2014 tittiatcoke@gmail.com
- Update to 4.12.1
  * KDE 4.12.1  release
  * See http://www.kde.org/announcements/announce-4.12.1.php
* Sat Dec 14 2013 tittiatcoke@gmail.com
- Update to 4.12.0
  * KDE 4.12.0  release
  * See http://www.kde.org/announcements/4.12.php
* Fri Nov 29 2013 tittiatcoke@gmail.com
- Update to 4.11.97
  * KDE 4.12 RC 1 release
  * See http://www.kde.org/announcements/announce-4.12-rc.php
* Sat Nov 23 2013 tittiatcoke@gmail.com
- Update to 4.11.95
  * KDE 4.12 Beta 3 release
  * See http://www.kde.org/announcements/announce-4.12-beta3.php
* Sat Nov 16 2013 tittiatcoke@gmail.com
- Update to 4.11.90
  * KDE 4.12 Beta 2 release
  * See http://www.kde.org/announcements/announce-4.12-beta2.php
* Sat Nov  9 2013 tittiatcoke@gmail.com
- Update to 4.11.80
  * KDE 4.12 Beta 1 release
  * See http://www.kde.org/announcements/announce-4.12-beta1.php
* Sat Nov  2 2013 tittiatcoke@gmail.com
- Update to 4.11.3
  * KDE 4.11.3 bugfix release
  * See http://www.kde.org/announcements/announce-4.11.3.php
* Sat Sep 28 2013 tittiatcoke@gmail.com
- Update to 4.11.2
  * KDE 4.11.2 bugfix release
  * See http://www.kde.org/announcements/announce-4.11.2.php
* Sun Sep  1 2013 tittiatcoke@gmail.com
- Update to 4.11.1
  * KDE 4.11.1 bugfix release
  * See http://www.kde.org/announcements/announce-4.11.1.php
* Thu Aug  8 2013 tittiatcoke@gmail.com
- Update to 4.11.0
  * KDE 4.11 Final release
  * See http://www.kde.org/announcements/4.11/
* Fri Jul 26 2013 hrvoje.senjan@gmail.com
- Update to 4.10.97
  * KDE 4.11 RC 2 release
  * See http://www.kde.org/announcements/announce-4.11-rc2.php
* Wed Jul 10 2013 cgiboudeaux@gmx.com
- Update to 4.10.95
  * KDE 4.11 RC 1 release
  * See http://www.kde.org/announcements/announce-4.11-rc1.php
* Thu Jun 27 2013 tittiatcoke@gmail.com
- Update to 4.10.90
  * KDE 4.11 Beta 1 release
  * See http://www.kde.org/announcements/announce-4.11-beta2.php
* Thu Jun 13 2013 tittiatcoke@gmail.com
- Update to 4.10.80
  * KDE 4.11 Beta 1 release
  * See http://www.kde.org/announcements/announce-4.11-beta1.php
* Sat Jun  1 2013 tittiatcoke@gmail.com
- Update to 4.10.4
  * Bugfix release
  * See http://www.kde.org/announcements/announce-4.10.4.php
  * resolves bnc#8122760
* Sat May  4 2013 tittiatcoke@gmail.com
- Update to 4.10.3
  * Bugfix release
  * See http://www.kde.org/announcements/announce-4.10.3.php
  * resolves bnc#818500
* Mon Apr  1 2013 tittiatcoke@gmail.com
- Update to 4.10.2 \n  * Bugfixes
* Sat Mar  2 2013 tittiatcoke@gmail.com
- Update to 4.10.1
  * Contains bug fixes. See http://www.kde.org/announcements/
  for more information
* Thu Jan 31 2013 tittiatcoke@gmail.com
- Update to 4.10.0
  * This is the final release for 4.10. Contains bugfixes
* Thu Jan 17 2013 tittiatcoke@gmail.com
- Update to 4.9.98
  * This is the third RC release for 4.10. Contains bugfixes
* Thu Jan  3 2013 tittiatcoke@gmail.com
- Update to 4.9.97
  * This is the second RC release for 4.10. Contains bugfixes
* Wed Dec 19 2012 tittiatcoke@gmail.com
- Update to 4.9.95
  * This is the first RC release for 4.10. Contains bugfixes
* Sat Dec  1 2012 tittiatcoke@gmail.com
- Update to 4.9.90
  * This is the second Beta release for 4.10. Contains bugfixes
* Sat Nov 24 2012 tittiatcoke@gmail.com
- Update to 4.9.80
  * See http://www.kde.org/announcements/announce-4.10-beta1.php
* Wed Nov 21 2012 cgiboudeaux@gmx.com
- Fix build on SLES (%%make_install is not expanded)
* Fri Nov  2 2012 tittiatcoke@gmail.com
- Update to 4.9.3
  * see http://kde.org/announcements/announce-4.9.3.php for details
* Sat Sep 29 2012 tittiatcoke@gmail.com
- Update to 4.9.2
  * see http://kde.org/announcements/announce-4.9.2.php for details
* Wed Sep  5 2012 dmueller@suse.com
- update to 4.9.1
  * see http://kde.org/announcements/4.9.1/ for details
* Mon Jul 30 2012 dmueller@suse.com
- update to 4.8.5
  * see http://kde.org/announcements/changelogs/changelog4_8_4to4_8_5.php for details
* Fri Jun  1 2012 dmueller@suse.com
- update to 4.8.4
  * see http://kde.org/announcements/changelogs/changelog4_8_3to4_8_4.php for details
* Sun Apr 29 2012 dmueller@suse.com
- update to 4.8.3
  * see http://kde.org/announcements/changelogs/changelog4_8_2to4_8_3.php for details
* Thu Mar 29 2012 dmueller@suse.de
- update to 4.8.2
  * see http://kde.org/announcements/changelogs/changelog4_8_1to4_8_2.php for details
* Fri Mar  2 2012 dmueller@suse.de
- update to 4.8.1
  * see http://kde.org/announcements/changelogs/changelog4_8_0to4_8_1.php for details
* Wed Jan 18 2012 dmueller@suse.de
- update to 4.8.0
  * first stable release of KDE 4.8 (only critical fixes over 4.7.98)
  * see http://kde.org/announcements/4.8/ for details
* Tue Jan 10 2012 dmueller@suse.de
- update to 4.7.98
  * RC2+ milestone release of KDE 4.8
  * see http://kde.org/announcements/4.8/ for details
* Wed Dec  7 2011 coolo@suse.com
- fix license to be in spdx.org format
* Fri Dec  2 2011 dmueller@suse.de
- update to 4.7.4
  * see http://kde.org/announcements/changelogs/changelog4_7_3to4_7_4.php for details
* Mon Nov 28 2011 idoenmez@suse.de
- Fix duplicate files
* Wed Nov  2 2011 dmueller@suse.de
- update to 4.7.3
  * see http://kde.org/announcements/changelogs/changelog4_7_2to4_7_3.php for details
* Fri Oct 28 2011 ctrippe@opensuse.org
- use proper icons for document and folder_html which fit the new
  theme (bnc#726207)
* Fri Oct  7 2011 ctrippe@opensuse.org
- install the document and folder_html icon also with 256px
* Sun Oct  2 2011 dmueller@suse.de
- update to 4.7.2
  * see http://kde.org/announcements/changelogs/changelog4_7_1to4_7_2.php for details
* Thu Sep 29 2011 idonmez@suse.com
- Add back our custom document icon, oxygen seems to be missing
  those, also re-add folder-html -> folder_html symlink,
  fixes bnc#720751
* Fri Sep  2 2011 dmueller@suse.de
- update to 4.7.1
  * Bugfixes over KDE 4.7.0
  * see http://kde.org/announcements/changelogs/changelog4_7_0to4_7_1.php for details
* Tue Aug 16 2011 idonmez@novell.com
- Fix -scalable sub-package but don't recommend it because its too
  big and not really useful.
* Tue Aug 16 2011 idonmez@novell.com
- Remove our custom document icons
* Fri Jul 22 2011 dmueller@suse.de
- update to 4.7.0
  * Small fixes over KDE 4.7 RC2
  * see http://kde.org/announcements/4.7 for details
* Thu Jul 21 2011 idonmez@novell.com
- Update to 4.6.95
* Fri Jul  1 2011 dmueller@suse.de
- update to 4.6.5
  * Bugfixes over KDE 4.6.5
  * see http://kde.org/announcements/changelogs/changelog4_6_4to4_6_5.php for details
* Fri Jun  3 2011 dmueller@suse.de
- update to 4.6.4
  * Bugfixes over KDE 4.6.3
  * see http://kde.org/announcements/changelogs/changelog4_6_3to4_6_4.php for details
* Wed May 18 2011 ctrippe@opensuse.org
- install the document and folder_html icon also with 256px (bnc#692450)
* Thu Apr 28 2011 dmueller@suse.de
- update to 4.6.3
  * Bugfixes over KDE 4.6.2
  * see http://kde.org/announcements/changelogs/changelog4_6_2to4_6_3.php for details
* Fri Apr  1 2011 dmueller@suse.de
- update to 4.6.2
  * Bugfixes over KDE 4.6.1
  * see http://kde.org/announcements/changelogs/changelog4_6_1to4_6_2.php for details
* Tue Mar  8 2011 dmueller@suse.de
- update to 4.6.1
  * Bugfixes over KDE 4.6.0
  * see http://kde.org/announcements/changelogs/changelog4_6_0to4_6_1.php for details
* Wed Jan 19 2011 dmueller@suse.de
- update to 4.6.0
  * For highlights, see http://kde.org/announcements/4.6
* Tue Jan  4 2011 dmueller@suse.de
- update to 4.5.95
  * KDE 4.6 RC2
  * no upstream changelog available.
* Wed Dec 22 2010 dmueller@suse.de
- update to 4.5.90
  * KDE 4.6 RC1
  * no upstream changelog available.
* Fri Dec  3 2010 dmueller@suse.de
- update to 4.5.85
  * KDE 4.6 Beta2
  * Final Beta before RC, various fixes from Beta1
  * no upstream changelog available.
* Fri Nov 19 2010 dmueller@suse.de
- update to 4.5.80
  * KDE 4.6 Beta1
  * no upstream changelog available.
* Thu Oct 28 2010 dmueller@suse.de
- update to 4.5.3
  * see http://kde.org/announcements/changelogs/changelog4_5_2to4_5_3.php for details
* Thu Sep 30 2010 dmueller@suse.de
- update to 4.5.2
  * see http://kde.org/announcements/changelogs/changelog4_5_1to4_5_2.php for details
* Fri Aug 27 2010 dmueller@suse.de
- update to 4.5.1
  * see http://kde.org/announcements/changelogs/changelog4_5_0to4_5_1.php for details
* Thu Jul 29 2010 dmueller@suse.de
- update to 4.5.0
  * KDE 4.5.0 final (version bump over RC3)
* Fri Jul 23 2010 dmueller@suse.de
- update to 4.4.95
  * KDE 4.5 RC3 (not announced)
  * critical fixes for 4.5.0 release
* Fri Jul 16 2010 dmueller@suse.de
- update to 4.4.93svn1149349
* Wed Jul  7 2010 dmueller@suse.de
- update to 4.4.5
  * bugfixes over 4.4.4
  * see http://kde.org/announcements/changelogs/changelog4_4_4to4_4_5.php for details
* Fri Jun 18 2010 dmueller@suse.de
- update to 4.4.4
  * bugfixes over 4.4.3
  * see http://kde.org/announcements/changelogs/changelog4_4_3to4_4_ 4.php for details
  * for most modules only version number as a change (4_4_BRANCH.diff already contained the diff
* Thu Apr 29 2010 dmueller@suse.de
- update to 4.4.3
  * bugfixes over 4.4.2
  * see http://kde.org/announcements/changelogs/changelog4_4_2to4_4_3.php for details
* Fri Mar 26 2010 dmueller@suse.de
- update to 4.4.2
  * bugfixes over 4.4.1
  * see http://kde.org/announcements/changelogs/changelog4_4_1to4_4_2.php for details
* Fri Feb 26 2010 dmueller@suse.de
- update to 4.4.1
  * bugfixes over 4.4.0
  * see http://kde.org/announcements/changelogs/changelog4_4_0to4_4_1.php for details
* Thu Feb  4 2010 dmueller@suse.de
- update to 4.4.0
  * Critical bugfixes only over 4.3.98
  * see http://www.kde.org/announcements/4.4/ for general overview
* Sun Jan 31 2010 dmueller@suse.de
- update to 4.3.98
  * see http://www.kde.org/announcements/announce-4.4-rc3.php for details
* Wed Jan 20 2010 dmueller@suse.de
- update to 4.3.95
  * see http://www.kde.org/announcements/announce-4.4-rc2.php for details
* Wed Jan 13 2010 llunak@novell.com
- split large icons (128x128 and up) to -large subpackage
* Wed Jan  6 2010 dmueller@suse.de
- update to 4.3.90
  * see http://www.kde.org/announcements/announce-4.4-rc1.php for details
* Fri Dec 18 2009 dmueller@suse.de
- update to 4.3.85
  * see http://www.kde.org/announcements/announce-4.4-beta2.php for details
* Fri Dec  4 2009 dmueller@suse.de
- update to 4.3.80:
  * see http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan
* Mon Nov  2 2009 dmueller@suse.de
- update to 4.3.3
  * see http://kde.org/announcements/changelogs/changelog4_3_2to4_3_3.php for details
* Fri Aug 28 2009 dmueller@suse.de
- update to 4.3.1
  * see http://kde.org/announcements/changelogs/changelog4_3_0to4_3_1.php for details
* Wed Jul 29 2009 dmueller@suse.de
- update to 4.3.0
  * see http://kde.org/announcements/4.3 for details
* Tue Jul 21 2009 dmueller@suse.de
- update to 4.2.98
* Thu Jul  9 2009 dmueller@suse.de
- update to 4.2.96
* Wed Jun 24 2009 dmueller@suse.de
- update to 4.2.95
* Wed Jun  3 2009 dmueller@suse.de
- update to 4.2.90
* Wed May 27 2009 dmueller@suse.de
- update to 4.2.88svn973768
* Thu May 14 2009 dmueller@suse.de
- update to 4.2.85 (KDE 4.3 Beta1)
* Fri May  1 2009 dmueller@suse.de
- update to 4.2.71
* Fri Feb 13 2009 dmueller@suse.de
- fix installation of extra oxygen icons
* Fri Jul 25 2008 dmueller@suse.de
- update to 4.1.0
- fix phonon-backend version
* Fri Jul 18 2008 dmueller@suse.de
- update to 4.0.99
* Wed Jul  9 2008 dmueller@suse.de
- update to 4.0.98
* Mon Jul  7 2008 dmueller@suse.de
- split-off phonon-backend-xine
* Mon Jul  7 2008 dmueller@suse.de
- update to 4.0.85
* Mon Jun 30 2008 dmueller@suse.de
- revert last change
* Mon Jun 30 2008 stbinner@suse.de
- fix file list
* Thu Jun 26 2008 dmueller@suse.de
- update to 4.0.84
* Mon Jun 23 2008 dmueller@suse.de
- update to 4.0.83
* Sun Jun 22 2008 dmueller@suse.de
- update to 4.0.82
* Thu Jun  5 2008 stbinner@suse.de
- fix huge Documents and public_html icons are blurry (bnc#396176)
* Mon Jun  2 2008 dmueller@suse.de
- oxygen style fixes:
  * render focus on radio buttons
  * don't apply a grey gradient to a blank screensaver (bnc#392792)
* Fri May 23 2008 stbinner@suse.de
- fix entries under "Applications" without kdebase3 (bnc#381855)
* Tue May 20 2008 llunak@suse.cz
- do not always default to kdesu remembering password (bnc#386531)
* Mon May 19 2008 stbinner@suse.de
- fix missing icons for /etc/skel directories (bnc#381701)
* Wed May 14 2008 llunak@suse.cz
- make the kde4 wrapper prefer actually KDE4 applications
* Wed May 14 2008 llunak@suse.cz
- support for KDE3 hotplug actions (bnc#378338)
* Tue May 13 2008 dmueller@suse.de
- update branch diff:
  * fix ksmserver/knotify4 deadlock causing an excessive
  login delay
* Fri May  9 2008 dmueller@suse.de
- branch diff update
  * new translations
* Tue May  6 2008 dmueller@suse.de
- update to 4.0.4
  * http://kde.org/announcements/changelog_4_0_3to4_0_4.php
* Mon Apr 28 2008 dmueller@suse.de
- disable video decoding when not needed (bnc#373181)
* Mon Apr  7 2008 dmueller@suse.de
- add ispell recommends (bnc#204734)
* Mon Mar 31 2008 dmueller@suse.de
- update to 4.0.3
* Tue Mar 11 2008 dmueller@suse.de
- update to the final 4.0.2 tarball
- update 4_0_BRANCH.diff
* Wed Feb 27 2008 dmueller@suse.de
- update to 4.0.2
* Wed Feb 27 2008 dmueller@suse.de
- update 4_0_BRANCH.diff, also apply it for oxygen-icon-theme
* Fri Feb  8 2008 stbinner@suse.de
- provide suse_help_viewer
* Thu Jan 31 2008 dmueller@suse.de
- update to 4.0.1
* Wed Jan 23 2008 stbinner@suse.de
- update 4_0_BRANCH.diff
* Sun Jan 20 2008 stbinner@suse.de
- update 4_0_BRANCH.diff
* Sat Jan  5 2008 stbinner@suse.de
- update to 4.0 release
* Wed Jan  2 2008 stbinner@suse.de
- update to 3.97.2
* Sun Dec 16 2007 dmueller@suse.de
- update to 3.97.1
* Fri Dec 14 2007 ro@suse.de
- added directory to filelist
* Wed Dec  5 2007 dmueller@suse.de
- update to 3.97.0
* Wed Dec  5 2007 dmueller@suse.de
- update to 3.96.3
* Sat Dec  1 2007 dmueller@suse.de
- update to 3.96.2
* Wed Nov 28 2007 dmueller@suse.de
- update to 3.96.1
* Thu Nov 15 2007 wstephenson@suse.de
- update to 3.96.0
* Mon Nov  5 2007 dmueller@suse.de
- fix permissions of kdesud
* Thu Nov  1 2007 dmueller@suse.de
- update to 3.95.1
* Wed Oct 31 2007 dmueller@suse.de
- own konqueror/dirtree related directories
* Wed Oct 31 2007 dmueller@suse.de
- update to 3.95.0
* Thu Oct 18 2007 dmueller@suse.de
- update to 3.94.1
* Wed Oct 17 2007 ro@suse.de
- fix build: add directory to filelist
* Mon Oct  8 2007 dmueller@suse.de
- update to KDE 4.0 Beta3
* Tue Oct  2 2007 stbinner@suse.de
- update to 3.93.0.svn720100
* Wed Sep 26 2007 stbinner@suse.de
- update to 3.93.0.svn717244
* Fri Sep 21 2007 stbinner@suse.de
- don't remove icons anymore after build check got fixed (#307017)
* Thu Sep 13 2007 stbinner@suse.de
- update to 3.93.0.svn712052
* Mon Sep 10 2007 dmueller@suse.de
- don't set $LANGUAGE in konsole (#305210)
* Sat Sep  1 2007 stbinner@suse.de
- update to KDE 4.0 Beta 2
* Thu Aug 23 2007 stbinner@suse.de
- update to 3.92.0.svn703920
* Thu Aug 16 2007 stbinner@suse.de
- update to 3.92.0.svn700775
* Wed Aug 15 2007 stbinner@suse.de
- fix more file conflicts
* Thu Aug  9 2007 stbinner@suse.de
- update to 3.92.0.svn697325
- fix file conflicts
* Sat Jul 28 2007 stbinner@suse.de
- update to KDE 4.0 Beta 1
* Thu Jul 19 2007 stbinner@suse.de
- don't show System Settings on non-KDE/KDE3 desktop
* Mon Jul 16 2007 stbinner@suse.de
- use the non generic lib version for libkonq
* Thu Jul 12 2007 stbinner@suse.de
- update to 3.91.0.svn672298
* Sat Jun 23 2007 stbinner@suse.de
- update to 3.90.1.svn679137
* Fri Jun  8 2007 stbinner@suse.de
- update to 3.90.1.svn670093
* Mon Jun  4 2007 stbinner@suse.de
- fix build
* Fri Jun  1 2007 coolo@suse.de
- simplify spec file by using macros from kde4-filesystem
* Wed May 23 2007 stbinner@suse.de
- run %%fdupes
- fix prefix of two bins
* Wed May 16 2007 olh@suse.de
- Buildrequires libusb -> libusb-devel
* Tue May 15 2007 stbinner@suse.de
- show .desktop files of kdebase4-runtime only in a KDE session
* Sat May 12 2007 stbinner@suse.de
- libkdeinit -> libkdeinit4
* Thu May 10 2007 stbinner@suse.de
- fix package file conflicts
* Mon May  7 2007 stbinner@suse.de
- initial package in abuild
